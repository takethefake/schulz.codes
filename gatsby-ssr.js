const React = require("react");

exports.onRenderBody = ({ setPreBodyComponents }) => {
  setPreBodyComponents([
    <script
      type="text/javascript"
      key="theme-initializer"
      src="/set-dark-mode.js"
    />,
  ]);
};
/*

 */
