import * as React from "react";
type ColorMode = "light" | "dark";
import { useLocalStorage } from "../hooks/useLocalStorage";

export const colorModeContext = React.createContext<{
  colorMode: ColorMode;
  toggleColorMode: () => void;
} | null>(null);

export const ColorModeProvider = ({
  children,
}: {
  children: React.ReactNode;
}) => {
  const [colorMode, setColorMode] = useLocalStorage("color-mode", "light");

  React.useEffect(() => {
    if (typeof document !== "undefined") {
      setColorMode(document.documentElement.className);
    }
  }, []);

  const toggleColorMode = () => {
    const newColorMode = colorMode === "light" ? "dark" : "light";
    window.localStorage.setItem("color-mode", newColorMode);
    setColorMode(newColorMode);
    document.documentElement.className = newColorMode;
  };

  return (
    <colorModeContext.Provider value={{ colorMode, toggleColorMode }}>
      {children}
    </colorModeContext.Provider>
  );
};

export const useColorMode = () => {
  const contextValue = React.useContext(colorModeContext);
  if (contextValue === null) {
    throw new Error("useColorMode must be used within a ColorModeProvider");
  }
  return contextValue;
};
