export const formatTagUrl = (tag: string) =>
  `/tags/${encodeURI(tag.toLowerCase().replace(" ", "-"))}`;
