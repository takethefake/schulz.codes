import React from "react";
import { useColorMode } from "../../providers/ColorModeProvider";
import { Header } from "../Header";
import { graphql, useStaticQuery } from "gatsby";
import { getImage } from "gatsby-plugin-image";
import { convertToBgImage } from "gbimage-bridge";
import BackgroundImage from "gatsby-background-image";
import { Box } from "../Box";
import {
  LandingImageContentStyles,
  LandingImageContentSubtextStyles,
  LandingImageContentTextStyles,
  LandingImageGradientStyles,
  LandingImageStyles,
} from "./LandingImage.css";

export const LandingImage = () => {
  const { colorMode } = useColorMode();
  const { lightImage, darkImage } = useStaticQuery(
    graphql`
      query {
        darkImage: file(relativePath: { eq: "darkBgImage.jpeg" }) {
          childImageSharp {
            gatsbyImageData(
              width: 3000
              placeholder: BLURRED
              formats: [AUTO, WEBP, AVIF]
            )
          }
        }
        lightImage: file(relativePath: { eq: "lightBgImage.jpeg" }) {
          childImageSharp {
            gatsbyImageData(
              width: 3000
              placeholder: BLURRED
              formats: [AUTO, WEBP, AVIF]
            )
          }
        }
      }
    `
  );

  // Use like this:
  const image = React.useMemo(
    () =>
      colorMode === "light"
        ? convertToBgImage(getImage(lightImage))
        : convertToBgImage(getImage(darkImage)),
    [colorMode]
  );
  return (
    <>
      <BackgroundImage {...image} className={LandingImageStyles} />
      <div className={LandingImageContentStyles}>
        <div className={LandingImageContentTextStyles}>Daniel Schulz</div>
        <div className={LandingImageContentSubtextStyles}>
          React, Testing & DevOps
        </div>
        <div className={LandingImageGradientStyles} />
      </div>
      <div
        style={{
          position: "absolute",
          top: "0px",
          width: "100%",
        }}
      >
        <Header />
      </div>
    </>
  );
};
