import { sprinkles } from "../../theme/sprinkles.css";
import { style } from "@vanilla-extract/css";

export const LandingImageStyles = style({
  width: "100%",
  height: "100vh",
  backgroundPosition: "top",
  position: "relative",
});

export const LandingImageGradientStyles = style({
  position: "absolute",
  bottom: "0px",
  left: "0",
  width: "100%",
  height: "20px",
  backgroundImage: "linear-gradient(to bottom,  rgba(0,0,0,0), var(--bgColor))",
});

export const LandingImageContentStyles = style({
  position: "absolute",
  top: "0px",
  height: "100vh",
  width: "100%",
  alignItems: "center",
  flexDirection: "column",
  display: "flex",
  justifyContent: "center",
});

export const LandingImageContentTextStyles = sprinkles({
  fontSize: {
    mobile: 10,
    tablet: 10,
    desktop: 12,
  },
});
export const LandingImageContentSubtextStyles = sprinkles({
  paddingBottom: 20,
  fontSize: {
    mobile: 4,
    tablet: 4,
    desktop: 4,
  },
});
