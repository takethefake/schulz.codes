import { HStack, VStack } from "./Stack";
import { ColorModeToggle } from "./ColorModeToggle";
import * as React from "react";
import { Link } from "gatsby";
import { Box } from "./Box";
import { LinkStyles } from "../theme/general.css";
import { Container } from "./Container";

export const Header = () => {
  return (
    <Container>
      <HStack gap={2} alignItems="center" h={20}>
        <Link to={`/`} className={LinkStyles}>
          Daniel Schulz
        </Link>
        <Box flex={1} />
        <Link to={`/blog`} className={LinkStyles}>
          Blog
        </Link>
        <ColorModeToggle h={5} w={5} />
      </HStack>
    </Container>
  );
};
