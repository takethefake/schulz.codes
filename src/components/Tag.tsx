import { Box } from "./Box";
import React from "react";

export const Tag = (props: Parameters<typeof Box>[0]) => {
  return <Box p={2} {...props} />;
};
