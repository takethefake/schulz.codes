import { useColorMode } from "../providers/ColorModeProvider";
import * as React from "react";
import { Box } from "./Box";
import { FaMoon, FaSun } from "react-icons/fa";

export const ColorModeToggle = (props: Parameters<typeof Box>[0]) => {
  const { colorMode, toggleColorMode } = useColorMode();
  return (
    <Box
      as={colorMode === "light" ? FaMoon : FaSun}
      color={{ lightMode: "gray900", darkMode: "gray100" }}
      onClick={toggleColorMode}
      cursor="pointer"
      aria-label={
        colorMode === "light" ? "Toggle Darkmode" : "Toggle Lightmode"
      }
      {...props}
    />
  );
};
