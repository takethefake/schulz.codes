import { ColorModeProvider } from "../providers/ColorModeProvider";
import "../theme/global.css";
import * as React from "react";
import { Box } from "./Box";
import { Footer } from "./Footer";

export const Page = ({ children }: { children: React.ReactNode }) => {
  return (
    <ColorModeProvider>
      <Box
        as={"main"}
        display={"flex"}
        flexDirection={"column"}
        minHeight="screenHeight"
      >
        {children}
        <Box __flex={1} w="full" />
        <Footer />
      </Box>
    </ColorModeProvider>
  );
};
