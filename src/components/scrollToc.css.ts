import { globalStyle, style } from "@vanilla-extract/css";

export const TocHeading = style({
  fontSize: "1.4rem",
  marginBottom: 0,
  fontWeight: 700,
  marginTop: "38px",
});

export const StickyToc = style({
  maxWidth: "46.25rem",
  margin: "0 auto",
  "@media": {
    "screen and (min-width: 80rem)": {
      position: "sticky",
      left: 0,
      top: 0,
      maxHeight: "85vh",
      overflow: "auto",
      margin: "0 0 0 -20rem",
      float: "left",
      maxWidth: "20rem",
      borderRadius: "0.2rem",
      paddingLeft: "1rem",
      // @ts-ignore
      "-webkit-overflow-scrolling": "touch",
    },
  },
});

globalStyle(`${StickyToc} ol`, {
  listStyle: "none",
  margin: 0,
  padding: "0.5rem 0",
});

globalStyle(`${StickyToc} li`, {
  margin: 0,
  fontWeight: "lighter",
  padding: "0.2rem 0.2rem 0.2rem 0",
});

globalStyle(`${StickyToc} li.is-current`, {
  fontWeight: 700,
});

//
//   li {
//
//   }
//
//
//
//     li {
//       padding: 0.4rem 0.4rem 0.4rem 0;
//     }
//   }
// `
