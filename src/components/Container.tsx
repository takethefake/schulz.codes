import React from "react";
import { Box } from "./Box";

export const Container = (props: Parameters<typeof Box>[0]) => {
  return (
    <Box
      __margin="0 auto"
      width={{
        mobile: "full",
        tablet: "containerMd",
        desktop: "containerLg",
      }}
      px={{
        mobile: 4,
        tablet: 0,
        desktop: 0,
      }}
      {...props}
    />
  );
};
