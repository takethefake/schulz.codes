import React from "react";
import { Link } from "gatsby";
import {
  FaXing,
  FaLinkedin,
  FaTwitter,
  FaGitlab,
  FaGithub,
} from "react-icons/fa";
import { Box } from "./Box";

export const Footer: React.FC = () => {
  return (
    <Box
      display="flex"
      justifyContent="center"
      alignItems="center"
      width="full"
      height={24}
    >
      © {new Date().getFullYear()} Daniel Schulz &nbsp;{" "}
      <a
        href="https://www.xing.com/profile/Daniel_Schulz153/cv"
        title="Xing"
        target="__blank"
      >
        <FaXing />
      </a>
      &nbsp;
      <a
        href="https://www.linkedin.com/in/daniel-schulz-a49952194/"
        title="Linked In"
        target="__blank"
      >
        <FaLinkedin />
      </a>
      &nbsp;
      <a href="https://github.com/takethefake" title="Gitlab" target="__blank">
        <FaGithub />
      </a>
      &nbsp;
      <a href="https://gitlab.com/takethefake" title="Gitlab" target="__blank">
        <FaGitlab />
      </a>
      <a
        href="https://twitter.com/takethefake"
        title="Twitter"
        target="__blank"
      >
        <FaTwitter />
      </a>
      &nbsp;
      <Link to="/datenschutz">Datenschutz</Link>
    </Box>
  );
};
