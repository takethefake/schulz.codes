import React from "react";
import { Box } from "./Box";

export const Stack = (props: Parameters<typeof Box>[0]) => {
  return <Box display="flex" {...props} />;
};

export const HStack = (
  props: Omit<Parameters<typeof Box>[0], "flexDirection" | "display">
) => {
  return <Stack flexDirection="row" {...props} />;
};

export const VStack = (
  props: Omit<Parameters<typeof Box>[0], "flexDirection" | "display">
) => {
  return <Stack flexDirection="column" {...props} />;
};
