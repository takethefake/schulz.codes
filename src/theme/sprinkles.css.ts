import { createSprinkles, defineProperties } from "@vanilla-extract/sprinkles";
import { spacing } from "./spacing";
import sizes from "./sizes";
import { LandingImageContentTextStyles } from "../components/LandingImage/LandingImage.css";
import colors from "./colors";

const responsiveProperties = defineProperties({
  conditions: {
    mobile: {},
    tablet: { "@media": "screen and (min-width: 768px)" },
    desktop: { "@media": "screen and (min-width: 1024px)" },
  },
  defaultCondition: "mobile",
  properties: {
    display: ["none", "flex", "block", "inline"],
    flexDirection: ["row", "column"],
    flex: [1, 2, 3, 4, 5],
    justifyContent: [
      "stretch",
      "flex-start",
      "center",
      "flex-end",
      "space-around",
      "space-between",
    ],
    alignItems: ["stretch", "flex-start", "center", "flex-end"],
    gap: spacing,
    paddingTop: spacing,
    paddingBottom: spacing,
    paddingLeft: spacing,
    paddingRight: spacing,
    width: sizes,
    height: sizes,
    minHeight: sizes,
    fontSize: sizes,
    lineHeight: sizes,

    marginTop: spacing,
    marginBottom: spacing,
    marginLeft: spacing,
    marginRight: spacing,
  },
  shorthands: {
    padding: ["paddingTop", "paddingBottom", "paddingLeft", "paddingRight"],
    p: ["paddingTop", "paddingBottom", "paddingLeft", "paddingRight"],
    px: ["paddingLeft", "paddingRight"],
    py: ["paddingTop", "paddingBottom"],
    placeItems: ["justifyContent", "alignItems"],
    margin: ["marginTop", "marginBottom", "marginLeft", "marginRight"],
    m: ["marginTop", "marginBottom", "marginLeft", "marginRight"],
    mx: ["marginLeft", "marginRight"],
    my: ["marginTop", "marginBottom"],
    h: ["height"],
    w: ["width"],
  },
});

export const semanticColors = {
  dark: {
    bgColor: "rgb(25, 25, 25)",
    textColor: "rgb(204, 204, 204)",
    primary: '#419eda',
  },
  light: {
    bgColor: "#D9D7D7",
    textColor: colors.gray900,
    primary: colors.green700,
  },
};

const usedColors = colors;

const colorProperties = defineProperties({
  conditions: {
    lightMode: { selector: ".light &" },
    darkMode: { selector: ".dark &" },
  },
  defaultCondition: "lightMode",
  properties: {
    color: usedColors,
    fill: usedColors,
    stroke: usedColors,
    background: usedColors,
  },

  shorthands: {
    bg: ["background"],
  },
});

export const sprinkles = createSprinkles(responsiveProperties, colorProperties);
// It's a good idea to export the Sprinkles type too
export type Sprinkles = Parameters<typeof sprinkles>[0];
