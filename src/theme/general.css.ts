import { style } from "@vanilla-extract/css";
import { semanticColors } from "./sprinkles.css";

export const LinkStyles = style({
  all: "unset",
  cursor: "pointer",
  selectors: {
    "&:hover": {
      color: "var(--primary)",
      textDecoration: "underline",
    },
  },
});
