import {  globalStyle } from "@vanilla-extract/css";
import { semanticColors } from "./sprinkles.css";

globalStyle("html, body", {
  margin: 0,
  fontFamily: "'Source Sans Pro',sans-serif",
  fontWeight: 400,
  fontFeatureSettings: '"kern", "liga", "clig", "calt"',
});

globalStyle("*", {
  boxSizing: "border-box",
});

globalStyle("h1,h2,h3,h4,h5,h6", {
  fontWeight: 200,
});

globalStyle("p, div,a", {
  fontSize:'18px',
  lineHeight:1.56
});

globalStyle('a',{
    color:'var(--primary)',
  textDecoration:'none',
})

globalStyle('a:hover',{
  textDecoration:'underline',
})

globalStyle("html.dark, .dark body", {
  color: semanticColors.dark.textColor,
  background: semanticColors.dark.bgColor,

  ...Object.fromEntries(
    Object.entries(semanticColors.dark).map(([key, value]) => {
      return [`--${key}`, value];
    })
  ),
});

globalStyle("html.light, .light body", {
  color: semanticColors.light.textColor,
  background: semanticColors.light.bgColor,

  ...Object.fromEntries(
    Object.entries(semanticColors.light).map(([key, value]) => {
      return [`--${key}`, value];
    })
  ),
});
