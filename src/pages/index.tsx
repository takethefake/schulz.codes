import * as React from "react";
import { Page } from "../components/Page";
import { LandingImage } from "../components/LandingImage/LandingImage";
import { graphql, PageProps } from "gatsby";
import { GatsbyImage, getImage } from "gatsby-plugin-image";
import { Container } from "../components/Container";
import { BlogEntry } from "../components/BlogEntry";
import SEO from "../components/Seo";

// markup
const IndexPage = ({ data }: PageProps<Queries.IndexBlogListQuery>) => {
  const profileImage = data.file?.childImageSharp?.gatsbyImageData
    ? getImage(data.file.childImageSharp.gatsbyImageData)
    : null;
  return (
    <Page>
      <SEO title={"Home"} />
      <LandingImage />
      <Container
        display="flex"
        flexDirection="column"
        alignItems="center"
        paddingBottom={24}
      >
        <h1>Hey, nice to meet you!</h1>
        {profileImage && (
          <GatsbyImage
            alt={"Profile Image of Daniel Schulz"}
            image={profileImage}
            style={{ borderRadius: "50%" }}
          />
        )}
<p>Welcome to schulz.codes, a software development company that specializes in web development and related technologies. With over 7 years of experience in the field, we have a strong focus on architecting and developing web applications using modern technologies such as React.js, TypeScript, GraphQL, and REST.</p>
<p>We also have expertise in other areas such as DevOps, Docker, Kubernetes, Serverless, AWS, and Git, which allows us to create scalable, reliable, and efficient systems that meet the needs of our clients. Our approach is to use cutting-edge tools and technologies to deliver high-quality results that drive business growth.</p>
<p>At schulz.codes, we believe in sharing our knowledge and improving processes. We are experts in Agile methods such as Scrum, which enable us to deliver high-quality software on time and within budget. We also place great emphasis on automated software testing, which helps us to ensure that our code is reliable and efficient. In addition, we have experience in IT technical writing, which enables us to communicate complex technical information in a clear and concise manner.</p>
<p>We value collaboration and work closely with our clients to develop solutions that meet their specific needs. Our team is quick-witted, highly committed, and effective at time management, which allows us to deliver results efficiently and effectively. We pride ourselves on our ability to work under pressure and in a team, and we're always willing to go the extra mile to ensure our clients' satisfaction.</p>
<p>Whether you're looking for development services, consultation, or public speaking, schulz.codes has the expertise and experience to help. Contact us to learn more about how we can help you achieve your business goals.</p>
        <h2 style={{ alignSelf: "flex-start" }}>latest blog posts</h2>
        <div style={{ alignSelf: "flex-start" }}>
          {data.allMdx.edges.map((edge) => (
            <BlogEntry key={edge.node.id} edge={edge} />
          ))}
        </div>
      </Container>
    </Page>
  );
};

export const pageQuery = graphql`
  query IndexBlogList {
    file(relativePath: { eq: "profile.jpg" }) {
      childImageSharp {
        gatsbyImageData(
          width: 200
          placeholder: BLURRED
          formats: [AUTO, WEBP, AVIF]
        )
      }
    }
    allMdx(
      limit: 3
      filter: { fileAbsolutePath: { regex: "content/blog/i" } }
    ) {
      edges {
        node {
          id
          slug
          frontmatter {
            title
            date
            path
            tags
          }

          timeToRead
          excerpt
        }
      }
    }
  }
`;

export default IndexPage;
