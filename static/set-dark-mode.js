"use strict";

(function () {
  // Update the current theme to either 'light' or 'dark'
  function setTheme(theme) {
    console.log(theme, theme === "dark");
    if (theme.replaceAll('"', "") === "dark") {
      console.log("Setting theme to dark");
      document.documentElement.className = "dark";
    } else {
      console.log("Setting theme to light");
      document.documentElement.className = "light";
    }
  }

  let colorMode;
  try {
    colorMode = localStorage.getItem("color-mode");
  } catch (e) {}
  console.log(colorMode);

  // Is there an Operating System Preference?
  const darkQuery = window.matchMedia("(prefers-color-scheme: dark)");
  setTheme(colorMode ?? (darkQuery.matches ? "dark" : "light"));
})();
