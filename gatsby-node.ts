import path from "path";

const { createFilePath } = require("gatsby-source-filesystem");
exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions;
  // We only want to operate on `Mdx` nodes. If we had content from a
  // remote CMS we could also check to see if the parent node was a
  // `File` node here
  if (node.internal.type === "Mdx") {
    const value = createFilePath({ node, getNode });
    const { fileAbsolutePath } = node;
    const prefix = fileAbsolutePath.includes("/content/blog") ? "/blog" : "";

    createNodeField({
      // Name of the field you are adding
      name: "slug",
      // Individual MDX node
      node,
      // Generated value based on filepath with "blog" prefix
      value: `${prefix}${value}`,
    });
  }
};

exports.createPages = async ({ actions, graphql, reporter }) => {
  const { createPage } = actions;
  const result = await graphql(`
    {
      allMdx(sort: { order: DESC, fields: [frontmatter___date] }, limit: 1000) {
        edges {
          node {
            id
            fields {
              slug
            }
          }
        }
      }
    }
  `);
  // Handle errors
  if (result.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query.`);
    return;
  }
  result.data.allMdx.edges.forEach(({ node }) => {
    if (node.fields && node.fields.slug) {
      console.log(node.fields.slug, node.id);
      createPage({
        // This is the slug we created before
        path: node.fields.slug,
        // This component will wrap our MDX content
        component: path.resolve(`./src/templates/blogTemplate.tsx`),
        // We can use the values in this context in
        // our page layout component
        context: {
          id: node.id,
        },
      });
    }
  });
  const articleTags = await graphql(`
    {
      allMdx(filter: { fileAbsolutePath: { regex: "content/blog/i" } }) {
        edges {
          node {
            id
            frontmatter {
              tags
            }
          }
        }
      }
    }
  `);
  const tagNodes = articleTags.data.allMdx.edges.reduce((prev, { node }) => {
    if (node.frontmatter && node.frontmatter.tags) {
      node.frontmatter.tags.forEach((tag) => {
        const lowerTag = tag.toLowerCase();
        if (!prev[lowerTag]) {
          prev[lowerTag] = [];
        }
        prev[lowerTag] = [...prev[lowerTag], node.id];
      });
    }
    return prev;
  }, {});

  const tags = Object.entries(tagNodes).map(([tag, nodes]) => {
    const slug = `/tags/${encodeURI(tag.toLowerCase().replace(" ", "-"))}`;
    createPage({
      // This is the slug we created before
      path: slug,
      // This component will wrap our MDX content
      component: path.resolve(`./src/templates/tagTemplate.tsx`),
      // We can use the values in this context in
      // our page layout component
      context: {
        tag,
        nodes,
      },
    });
    return { path: slug, label: tag };
  });

  createPage({
    // This is the slug we created before
    path: `/tags`,
    // This component will wrap our MDX content
    component: path.resolve(`./src/templates/tagOverviewTemplate.tsx`),
    // We can use the values in this context in
    // our page layout component
    context: {
      tags,
    },
  });
};
